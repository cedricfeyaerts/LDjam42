# Crucifix and Tentacle

Our submission for the game jam [#LDjam41](https://ldjam.com/events/ludum-dare/42). 

The theme was **Running out of space**. Another game in Unity and Adobe Animate.

## what's a game jam?

* A theme
* 72 hours (minus sleep and day job on monday)
* 4 dudes
* a lot of pizza
* a bit beer (just to celebrate)

... And at the end a working game.

## Something about tentacle...

First and foremost don't juge us too hard on the theme. It wasn't meant to be taken seriously. 

## Where to play

https://cedricfey.itch.io/crucifix-and-tentacle-mk1

## How to play? 

WASD for moving and click to shoot.

You can hold the click button pressed.  (But then you won't hear the sound of the protagonist, yes another bug).

If the enemy touch you the darkness comes closer. If you hit the darkness to many time you dieeee.

Like ou last game it's very hard. Too hard! It's a shame though. Because if you could hold on 5 min the final boss comes. 
