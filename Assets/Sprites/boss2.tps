<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0091.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0092.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0093.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0094.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0095.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0096.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0097.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0098.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0099.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0100.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0101.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0102.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0103.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0104.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0105.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0106.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0107.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0108.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0109.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0110.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0111.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0112.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0113.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0114.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0115.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0116.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0117.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0118.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0119.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0120.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0121.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0122.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0123.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0124.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0125.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0126.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0127.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0128.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0129.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0130.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0131.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0132.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0133.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0134.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0135.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0136.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0137.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0138.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0139.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0140.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0141.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0142.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0143.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0144.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0145.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0146.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0147.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0148.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0149.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0150.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0151.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0152.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0153.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0154.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0155.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0156.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0157.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0158.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0159.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0160.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0161.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0162.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0163.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0164.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0165.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0166.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0167.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0168.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0169.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0170.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0171.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0172.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0173.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0174.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0175.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0176.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0177.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0178.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0179.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0180.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss2/boss0181.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,77,320,153</rect>
                <key>scale9Paddings</key>
                <rect>160,77,320,153</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../assets_ldjam42/assets/boss2</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
