<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>boss1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0001.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0002.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0003.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0004.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0005.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0006.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0007.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0008.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0009.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0010.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0011.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0012.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0013.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0014.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0015.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0016.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0017.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0018.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0019.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0020.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0021.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0022.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0023.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0024.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0025.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0026.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0027.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0028.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0029.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0030.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0031.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0032.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0033.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0034.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0035.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0036.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0037.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0038.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0039.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0040.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0041.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0042.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0043.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0044.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0045.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0046.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0047.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0048.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0049.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0050.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0051.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0052.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0053.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0054.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0055.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0056.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0057.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0058.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0059.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0060.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0061.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0062.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0063.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0064.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0065.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0066.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0067.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0068.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0069.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0070.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0071.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0072.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0073.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0074.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0075.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0076.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0077.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0078.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0079.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0080.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0081.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0082.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0083.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0084.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0085.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0086.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0087.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0088.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0089.png</key>
            <key type="filename">../../../assets_ldjam42/assets/boss/boss0090.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,77,320,153</rect>
                <key>scale9Paddings</key>
                <rect>160,77,320,153</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../assets_ldjam42/assets/boss</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
