<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>idle.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0001.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0002.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0003.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0004.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0005.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0006.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0007.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0008.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0009.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0010.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0011.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0012.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0013.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0014.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0015.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0016.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0017.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0018.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0019.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0020.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0021.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0022.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0023.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0024.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0025.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0026.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0027.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0028.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0029.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0030.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0031.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0032.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0033.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0034.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0035.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0036.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0037.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0038.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0039.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0040.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0041.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0042.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0043.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0044.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0045.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0046.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0047.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0048.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0049.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0050.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0051.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0052.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0053.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0054.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0055.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0056.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0057.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0058.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0059.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0060.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0061.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0062.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0063.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0064.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0065.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0066.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0067.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0068.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0069.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0070.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0071.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0072.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0073.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0074.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0075.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0076.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0077.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0078.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0079.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0080.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0081.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0082.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0083.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0084.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0085.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0086.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0087.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0088.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0089.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0090.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0091.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0092.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0093.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0094.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0095.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0096.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0097.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0098.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0099.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0100.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0101.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0102.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0103.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0104.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0105.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0106.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0107.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0108.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0109.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0110.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0111.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0112.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0113.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0114.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0115.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0116.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0117.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0118.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0119.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0120.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0121.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0122.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0123.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0124.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0125.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0126.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0127.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0128.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0129.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0130.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0131.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0132.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0133.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0134.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0135.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0136.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0137.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0138.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0139.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0140.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0141.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0142.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0143.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0144.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0145.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0146.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0147.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0148.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0149.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0150.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0151.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0152.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0153.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0154.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0155.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0156.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0157.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0158.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0159.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0160.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0161.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0162.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0163.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0164.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0165.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0166.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0167.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0168.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0169.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0170.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0171.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0172.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0173.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0174.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0175.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0176.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0177.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0178.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0179.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0180.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0181.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0182.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0183.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0184.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0185.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0186.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0187.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0188.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0189.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0190.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0191.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0192.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0193.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0194.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0195.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0196.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0197.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0198.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0199.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0200.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0201.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0202.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0203.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0204.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0205.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0206.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0207.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0208.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0209.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0210.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0211.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0212.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0213.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0214.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0215.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0216.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0217.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0218.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0219.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0220.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0221.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0222.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0223.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0224.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0225.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0226.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0227.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0228.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0229.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0230.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0231.png</key>
            <key type="filename">../../../assets_ldjam42/assets/idle/idle0232.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,23,35,45</rect>
                <key>scale9Paddings</key>
                <rect>17,23,35,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../assets_ldjam42/assets/idle</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
