<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../assets_ldjam42/assets/gear.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0001.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0002.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0003.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0004.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0005.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0006.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0007.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0008.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0009.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0010.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0011.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0012.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0013.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0014.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0015.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0016.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0017.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0018.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0019.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0020.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0021.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0022.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0023.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0024.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0025.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0026.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0027.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0028.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0029.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0030.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0031.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0032.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0033.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0034.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0035.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0036.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0037.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0038.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0039.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0040.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0041.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0042.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0043.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0044.png</key>
            <key type="filename">../../../assets_ldjam42/assets/gear_anim/gear0045.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,81,81</rect>
                <key>scale9Paddings</key>
                <rect>40,40,81,81</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../assets_ldjam42/assets/gear_anim</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>unity-importer-4.6.1</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
