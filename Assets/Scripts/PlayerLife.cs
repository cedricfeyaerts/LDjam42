using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour {

    public int HitPoint = 5;
    public float Light = 1.0f;
    public Transform CircleRopeTransform;
    private int _CurrentHitPoint;
    private float _CurrentLight;
    private Transform _impact;
    private CircleRopeComponent rope;
    private Transform _transform;

    private AudioSource AudioSource;
    public AudioClip damageClip;
    public AudioClip darknessClip;
    public AudioClip dieClip;
    private bool rd = false;


	// Use this for initialization
	void Start () {
        _CurrentHitPoint = HitPoint;
        _CurrentLight = Light;
        _transform = gameObject.GetComponent<Transform>();
        _impact = this.gameObject.transform.GetChild(0);
        rope = CircleRopeTransform.GetComponent<CircleRopeComponent>();
        AudioSource =  gameObject.GetComponent<AudioSource>();

	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate()
    {
        restrictToScreen();
        if (IsDead())
            Die();
	}

    void restrictToScreen()
    {
        Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        Vector2 screenOrigo = Camera.main.ScreenToWorldPoint(Vector2.zero);
         Vector3 pos = _transform.position;
        if (pos.x > screenBounds.x || pos.x < screenOrigo.x || pos.y > screenBounds.y || pos.y < screenOrigo.y)
        {
            Die();
        }
    }

    public bool IsDead()
    {
        return _CurrentHitPoint < 0;
    }

    public void Die()
    {
        if(rd){
            return;
        }
        rope.reduceLight(0);
        AudioSource.clip = dieClip;
        AudioSource.Play();
        transform.GetComponent<Renderer>().enabled = false;
        InvokeRepeating("restart", 3.0f, 100);
        rd = true;
    }

    private void restart(){
        // Application.LoadLevel(Application.loadedLevel);
        SceneManager.LoadScene("Intro");
    }

    public void DealDamage(int Damages){

        AudioSource.clip = damageClip;
        AudioSource.Play();
        _CurrentHitPoint -= Damages;
        float ratioLife = (float)_CurrentHitPoint / (float)HitPoint;
        gameObject.GetComponent<SpriteRenderer>().color=new Color(1, ratioLife, ratioLife, 1);
        _impact.GetComponent<Animator>().SetTrigger("hit");
    }

    public void DealDarkness(float darkness){

        AudioSource.clip = darknessClip;
        AudioSource.Play();
        _CurrentLight -= darkness;
        rope.reduceLight(_CurrentLight);
        if(_CurrentLight<0){
            _CurrentHitPoint = 0;
        }
    }

    public void DealLight(float light){
        _CurrentLight = Mathf.Clamp01(_CurrentLight + light);
        rope.reduceLight(_CurrentLight);
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        if (IsDead()){
            return ;
        }
        if (col.gameObject.tag == "Skull")
        {
            DealDarkness(0.05f);
            Destroy(col.gameObject);
        }
        if (col.gameObject.tag == "Eye"){
            DealDarkness(0.2f);
        }
        if (col.gameObject.tag == "Tentacle"){
            DealDarkness(0.2f);
        }
        if (col.gameObject.tag == "Ghost"){
            DealDarkness(0.1f);
        }if (col.gameObject.tag == "Darkness"){
            DealDamage(1);
        }
    }
}
