﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MovableComponent {

	    private bool _Dying;

    public float HitPoint;
    public float CurrentHitPoint;
    public Transform Player;
    private AudioSource AudioSource;
    public AudioClip dieClip;

	protected override void Start () {
        base.Start();
        CurrentHitPoint = HitPoint;
        AudioSource =  gameObject.GetComponent<AudioSource>();
        gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update () {

	}


    public virtual void FixedUpdate()
	{
        if (IsDead())
            Die();
	}

    public void DealDamage(float damage)
    {
        CurrentHitPoint -= damage;
    }

    public bool IsDead()
    {
        return CurrentHitPoint < 0;
    }

	public void Die()
	{
        if (_Dying)
            return;
        _Dying = true;
        destroy();        
	}

    void destroy(){
         Time.timeScale = 0;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Bullet"){
            DealDamage(1);
            Destroy(col.gameObject);
        }
        if (col.gameObject.tag == "Player"){
            DealDamage(1);
        }
    }


}
