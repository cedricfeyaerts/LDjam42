﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu_StartButton : MonoBehaviour {

    private Button _Button;

	// Use this for initialization
	void Start () {
        _Button = GetComponent<Button>();
        _Button.onClick.AddListener(() => OnClick());
	}
	
	// Update is called once per frame
	void Update () {
		
    }

    void OnClick()
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            var scene = SceneManager.GetSceneAt(i);
        }
        SceneManager.LoadScene("Main");
    }
}
