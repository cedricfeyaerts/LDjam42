﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemy : MovableComponent {

    private bool _Dying;

    public float HitPoint;
    public float CurrentHitPoint;
    public float lightValue = 0.01f;
    public Transform Player;
    private PlayerLife _playerLife;
    private AudioSource AudioSource;
    public AudioClip dieClip;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        CurrentHitPoint = HitPoint;
        AudioSource =  gameObject.GetComponent<AudioSource>();
        _playerLife = Player.GetComponent<PlayerLife>();
	}

	// Update is called once per frame
	void Update () {

	}

    public virtual void FixedUpdate()
	{
        if (IsDead())
            Die();
	}

    public void DealDamage(float damage)
    {
        CurrentHitPoint -= damage;
    }

    public bool IsDead()
    {
        return CurrentHitPoint < 0;
    }

	public void Die()
	{
        if (_Dying)
            return;
        _Dying = true;
        // _RigidBody.collider2D.destroy = false;
        transform.GetComponent<Collider2D>().enabled = false;
        AudioSource.clip = dieClip;
        AudioSource.Play();
        transform.GetComponent<Renderer>().enabled = false;
        InvokeRepeating("destroy", 0.5f, 100);
        _playerLife.DealLight(lightValue);
	}

    void destroy(){
        Destroy(this.gameObject);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Bullet"){
            DealDamage(1);
            Destroy(col.gameObject);
        }
        if (col.gameObject.tag == "Player"){
            DealDamage(1);
        }
    }
}
