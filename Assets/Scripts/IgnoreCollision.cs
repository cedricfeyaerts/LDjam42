﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour {

    private Rigidbody2D _Rigidbody;

    public string Tag;

	// Use this for initialization
	void Start () {
        _Rigidbody = GetComponent<Rigidbody2D>();
        var rigidbodies = FindObjectsOfType<Rigidbody2D>();
        foreach (var rigidbody in rigidbodies.Where(r => r.GetComponent<IgnoreCollision>() != null && r.GetComponent<IgnoreCollision>().Tag == Tag))
        {
            Physics2D.IgnoreCollision(rigidbody.GetComponent<Collider2D>(), _Rigidbody.GetComponent<Collider2D>());
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
