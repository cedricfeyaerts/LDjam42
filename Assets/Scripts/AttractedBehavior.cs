﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttractedBehavior : MonoBehaviour {

    public Rigidbody2D Rigidbody { get; private set; }
    public List<Rigidbody2D> Attractors;

    public AttractedBehavior()
    {
        Attractors = new List<Rigidbody2D>();
    }

	// Use this for initialization
	void Start () {
        Rigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void FixedUpdate()
	{
        if (Attractors == null)
            return;
        
        var attractors = Attractors.Select(r => r != null ? r.GetComponent<AttractorComponent>() : null).OfType<AttractorComponent>().ToList();
        foreach (var attractor in attractors)
        {
            var distance = Vector2.Distance(attractor.Rigidbody.position, Rigidbody.position);
            var vector = new Vector2(attractor.Rigidbody.position.x - Rigidbody.position.x, attractor.Rigidbody.position.y - Rigidbody.position.y);
            if (distance <= attractor.AttractionTreshold && attractor.AttractionTreshold > 0)
            {
                var velocity = Vector3.Project(Rigidbody.velocity, vector);
                velocity = velocity * -1;

                var scale = (attractor.AttractionTreshold - distance) / attractor.AttractionTreshold;
                velocity = velocity * scale;

                Rigidbody.AddForce(velocity);
            }
            else
            {
                var scaling = (float)Math.Pow(attractor.DistanceScaling, distance);
                var force = distance * attractor.Force * scaling;
                if (attractor.MaxForce > 0)
                    force = Mathf.Min(force, attractor.MaxForce);
                vector.Normalize();
                Rigidbody.AddForce(vector * force);
            }
        }
	}
}
