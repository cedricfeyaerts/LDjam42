﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyGhost : Ennemy {

    public float MovementSpeed;

	// Use this for initialization
	protected override void Start () {
        base.Start();
	}

	// Update is called once per frame
	void Update () {

	}

	public override void FixedUpdate()
    {
        base.FixedUpdate();

        var direction = new Vector2(Player.position.x - _RigidBody.position.x, Player.position.y - _RigidBody.position.y);

        if (direction.x > 0)
            GetComponent<SpriteRenderer>().flipX = true;
        else
            GetComponent<SpriteRenderer>().flipX = false;

        Move(_RigidBody, direction, MovementSpeed);
	}
}
