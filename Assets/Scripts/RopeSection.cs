using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RopeSection {
    public Vector3 position;
    public Vector3 velocity;

    //To write RopeSection.zero
    public static readonly RopeSection zero = new RopeSection(Vector3.zero);

    public RopeSection(Vector3 position)
    {
        this.position = position;

        this.velocity = Vector3.zero;
    }
}