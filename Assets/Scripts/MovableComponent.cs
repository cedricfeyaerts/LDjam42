﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableComponent : MonoBehaviour
{
    protected static readonly float EPSILON = 0.1f;

    protected Rigidbody2D _RigidBody;
    protected virtual float _Deceleration { get { return 0.9f; } }

    public Vector2 SpawnLocation;

    // Use this for initialization
    protected virtual void Start()
    {
        _RigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move(Rigidbody2D rb, Vector2 direction, float speed)
	{
        rb.velocity = direction.normalized * speed;
	}

	public bool WasStatic()
    {
        return false;
    }
}
