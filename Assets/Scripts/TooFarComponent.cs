﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooFarComponent : MonoBehaviour {
	public float distanceFromPlayer = 30f;

  	private Transform t;

	// Use this for initialization
	void Start () {
		t = this.GetComponent<Transform>();
	}

	// Update is called once per frame
	void Update () {
		GameObject player = GameObject.Find("Player");
		if (player != null && (player.transform.position - t.position).magnitude > distanceFromPlayer) {
			Debug.Log("removing bullet");
			DestroyImmediate(gameObject);
		}
	}
}
