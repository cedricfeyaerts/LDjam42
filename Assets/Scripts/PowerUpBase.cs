﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class PowerUpBase : MonoBehaviour {

    private float _nextFire;

    protected Rigidbody2D _Rigidbody;

    public float FireRate;

    public virtual bool Fire(Vector2 direction)
    {
        if (Time.time < _nextFire)
            return false;
        _nextFire = Time.time + FireRate;

        return true;

    }

    // Use this for initialization
    public virtual void Start ()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        var powerUps = GetComponents<PowerUpBase>().Where(p => p != this).ToList();
        powerUps.ForEach(r => Destroy(r));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
