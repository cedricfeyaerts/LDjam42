﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnnemiesWave : MonoBehaviour {

    private List<GameObject> _Ennemies;

    public List<GameObject> Ennemies;
    public List<Vector2> SpawnLocations;
    public float SpawnTime;
    public Transform Player;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpawnWave()
    {
        for (int index = 0; index < Ennemies.Count(); index++)
        {
            var go = Ennemies[index];
            var ennemy =  go.GetComponent<Ennemy>();
            var spawnLocation = index < SpawnLocations.Count() ? SpawnLocations[index] : new Vector2(0, 0);
            var copy = Instantiate(go, spawnLocation, Quaternion.identity);
            _Ennemies.Add(copy);
            copy.GetComponent<Ennemy>().Player = Player;
        }
    }
}
