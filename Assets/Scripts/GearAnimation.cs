﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearAnimation : MonoBehaviour {

	private static readonly float MINIMUM_ANGULAR = 0.3f;
	private static readonly float MAXIMUM_ANGULAR = 1f;
	private Transform _Transform;
	private float angular;

	// Use this for initialization
	void Start () {
		_Transform = GetComponent<Transform>();
		int direction = (Random.value > 0.5) ? 1 : -1;
		angular = ( Random.value * (MAXIMUM_ANGULAR - MINIMUM_ANGULAR) + MINIMUM_ANGULAR ) * direction;
		float scale = 1 + (Random.value * 0.5f);

		transform.localScale = new Vector3(scale, scale, scale);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate( new Vector3( 0, 0, angular ) );
	}
}
