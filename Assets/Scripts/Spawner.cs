﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public float ghostRadius = 10.0f;
    public Transform ghost;
    public float tentacleRadius = 1.0f;
    public Transform tentacle;
    public float eyeRadius = 1.0f;
    public Transform eye;
    public Transform boss;
    public Transform player;
    
    private float _time;
    private float _nextWaveGhost;
    private float _ghostInterval;    
    private float _nextWaveTentacle;
    private float _tentacletInterval;   
     private float _nextWaveEye;
    private float _eyeInterval;


	// Use this for initialization
	void Start () {
        _time = Time.time;
	}

	// Update is called once per frame
	void FixedUpdate () {
        if (Time.time - _time < 45){
            _ghostInterval = 1.0f;
            _tentacletInterval = 20.0f;
            _eyeInterval = 100.0f;
        }else if (Time.time - _time < 90){
            _ghostInterval = 0.5f;
            _tentacletInterval = 10.0f;
            _eyeInterval = 59.0f;
        }else if (Time.time - _time < 135){
            _ghostInterval = 1f;
            _tentacletInterval = 2.0f;
            _eyeInterval = 20.0f;
        }else if (Time.time - _time < 180){
            _ghostInterval = 0.2f;
            _tentacletInterval = 10.0f;
            _eyeInterval = 10.0f;
        }else{
            boss.gameObject.SetActive(true);
            _ghostInterval = 1.0f;
            _tentacletInterval = 20.0f;
            _eyeInterval = 1000.0f;
        }
        TrySpawnNextWave();
    }

    void  TrySpawnNextWave(){
        if (Time.time > _nextWaveGhost)
        {
            Spawn(ghost, ghostRadius);
            _nextWaveGhost = Time.time + _ghostInterval;
        }
        if (Time.time > _nextWaveTentacle)
        {
            Spawn(tentacle, ghostRadius);
            _nextWaveTentacle = Time.time + _tentacletInterval;

        }
        if (Time.time > _nextWaveEye)
        {
            Spawn(eye, ghostRadius);
            _nextWaveEye = Time.time + _eyeInterval;
        }
    }

    void Spawn(Transform prefab, float radius) {
        Vector2 position = Random.insideUnitCircle.normalized * radius;
        Transform transform = Instantiate(prefab, new Vector2(player.position.x + position.x ,player.position.y + position.y), Quaternion.identity);

        var ennemy = transform.GetComponent<Ennemy>();
        print(ennemy);
        if (ennemy){
            ennemy.Player = player;
        }
        var weaponAuto = transform.GetComponent<WeaponAuto>();
        print(weaponAuto);
        if (weaponAuto){
            weaponAuto.player = player;
        }

    }
    void SpawnX(int x,Transform transform, float radius) {
        for (int i = 0; i < x; i++){
            Spawn(transform, radius);
        }
    }

}
