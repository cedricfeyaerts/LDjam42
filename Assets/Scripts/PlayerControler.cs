﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MovableComponent {

    private ReticuleViseeBase _ReticuleVisee;
    private WeaponBase _Weapon;
    private PowerUpBase _PowerUp;
    private Animator _animator;

    private Vector2 _speed;
    private Transform _transform;
    private Vector2 _speedVector;

    public float MovementSpeed;
    private AudioSource AudioSource;
    public AudioClip shootClip;

	// Use this for initialization
	protected override void Start () {
        base.Start();

        _ReticuleVisee = GetComponent<ReticuleViseeBase>();
        _Weapon = GetComponent<WeaponBase>();
        _PowerUp = GetComponent<PowerUpBase>();
        _animator = GetComponent<Animator>();
        _transform = GetComponent<Transform>();
        _speedVector = Vector2.zero;
        AudioSource =  gameObject.GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void FixedUpdate ()
    {
        var l_horizontal = Input.GetAxis("P1_LHorizontal");
        var l_vertical = Input.GetAxis("P1_LVertical") * -1;
        bool moving = false;
        GetComponent<SpriteRenderer>().flipX = false;
        transform.rotation = Quaternion.identity;
        
        if (Input.GetButton("Right")){
            _speed = Vector2.right;
            moving = true;
            _animator.SetTrigger("run_right");
        }else if (Input.GetButton("Left")){
            _speed = Vector2.left;
            moving = true;
            _animator.SetTrigger("run_right");
            GetComponent<SpriteRenderer>().flipX = true;
        }else if(Input.GetButton("Down")){
            _speed = Vector2.down;
            moving = true;
            _animator.SetTrigger("run_down");
        }else if (Input.GetButton("Up")){
            _speed = Vector2.up;
            moving = true;
            _animator.SetTrigger("run_up");
        }
        
        if (Input.GetButton("Up") && Input.GetButton("Right"))
        {
            _speed = new Vector2(1.0f, 1.0f);
        }
        else if (Input.GetButton("Up") && Input.GetButton("Left"))
        {
            _speed = new Vector2(-1.0f, 1.0f);
        }
        if (Input.GetButton("Down") && Input.GetButton("Right"))
        {
            _speed = new Vector2(1.0f, -1.0f);
        }
        if (Input.GetButton("Down") && Input.GetButton("Left"))
        {
            _speed = new Vector2(-1.0f, -1.0f);
        }

        if (Input.GetButton("Fire_1"))
            Fire();

        if (Input.GetButton("Fire_2"))
            PowerUp();

        _animator.SetBool("running", moving);

        if (moving){
            _speed.Normalize();
            _speedVector = _speed * MovementSpeed;
        }else{
            _speedVector = new Vector2(_speedVector.x*0.8f,_speedVector.y*0.8f);
        }
        _transform.position = new Vector2(_transform.position.x + _speedVector.x, _transform.position.y + _speedVector.y );
    }



    // public void MovePlayer(float l_horizontal, float l_vertical)
    // {
    //     var vector = new Vector2(l_horizontal, l_vertical);

    //     if (System.Math.Abs(vector.x) > EPSILON || System.Math.Abs(vector.y) > EPSILON)
    //     {
    //         //Si le joueur etait imobile on lui donne une vitesse minimum dans la direction donnée
    //         if (WasStatic())
    //         {
    //             Move(_RigidBody, vector, MovementSpeed);
    //         }
    //         //Sinon on ajuste sa vitesse
    //         else
    //         {
    //             //Pour le moment on s'en fou, on va le faire aller a la vitesse minimum dans la direction 
    //             //souhaité
    //             Move(_RigidBody, vector, MovementSpeed);
    //         }
    //         // _animator.SetBool("running", true);
    //         UpdateDirectionAnimator(vector);
    //     }
    //     //Sinon on le fait ralentir
    //     else
    //     {
    //         Move(_RigidBody, _RigidBody.velocity, _Deceleration);
    //         // _animator.SetBool("running", false);
    //         UpdateDirectionAnimator(new Vector2(0, -1));
    //     }
    // }

    // private void UpdateDirectionAnimator(Vector2 direction)
    // {
    //     var angleSection = Mathf.PI / 2;
    //     var vector = new Vector2(Mathf.Cos(Mathf.PI/4), Mathf.Sin(Mathf.PI/4));
    //     var angle = Vector2.Angle(vector, direction);
    //     var section = angle / angleSection;
    //     // if (section < 1)
    //     //     _animator.SetTrigger("run_down");
    //     // else if (section < 2)
    //     //     _animator.SetTrigger("run_left");
    //     // else if (section < 3)
    //     //     _animator.SetTrigger("run_up");
    //     // else
    //     //     _animator.SetTrigger("run_right");
    // }

    public void Fire()
    {
        var vector = _ReticuleVisee.Direction();
        _Weapon.Fire(vector);
        AudioSource.clip = shootClip;
        AudioSource.Play();
    }

    public void PowerUp()
    {
        var vector = _ReticuleVisee.Direction();
        _PowerUp.Fire(vector);
    }
}
