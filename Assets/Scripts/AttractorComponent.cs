﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractorComponent : MonoBehaviour {

    public Rigidbody2D Rigidbody { get; private set; }

    public float Force;
    public float DistanceScaling;
    public float MaxForce;
    public float AttractionTreshold;

    public AttractorComponent()
    {
        Force = 1.0f;
    }

	// Use this for initialization
	void Start () {
        Rigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
