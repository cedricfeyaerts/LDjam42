﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAuto : WeaponBase {


	public float delay = 1.0f;
	public Transform player;
	private Transform _transform;
	public AudioClip shootClip;
	private AudioSource _audio;

	// Use this for initialization
	void Start () {
		base.Start();
		_transform = GetComponent<Transform>();
		Debug.Log(Projectil);
		InvokeRepeating("AutoFire", delay, FireRate);
		_audio = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void AutoFire () {
		Vector3 direction = new Vector3(player.transform.position.x - _transform.position.x, player.transform.position.y  - _transform.position.y);
		_audio.clip = shootClip;
		_audio.Play();
		Fire(direction);
	}
}
