﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnnemiesSpawner : MonoBehaviour {

    private bool _Started;
    private float _NextWave;
    private EnnemiesWave _CurrentWave;

    public List<GameObject> Waves;
    public Transform Player;

	// Use this for initialization
	void Start () {
        StartSpawning();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{
        if (!_Started)
            return;

        TrySpawnNextWave();
	}

    public void StartSpawning()
    {
        _Started = true;

        if (Waves.Any())
            _NextWave = Waves.First().GetComponent<EnnemiesWave>().SpawnTime;
    }

    public void TrySpawnNextWave()
    {
        if (Time.time > _NextWave)
        {
            SpawnNextWave();
        }
    }

    public void SpawnNextWave()
    {
        if (!Waves.Any())
            return;
        
        var nextWave = Waves.FirstOrDefault().GetComponent<EnnemiesWave>();
        nextWave.Player = Player;
        nextWave.SpawnWave();
        _CurrentWave = nextWave;
        Waves.Remove(Waves.First());

        if (Waves.Any())
            _NextWave = Time.time + Waves.FirstOrDefault().GetComponent<EnnemiesWave>().SpawnTime;
    }
}
