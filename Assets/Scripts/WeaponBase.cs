﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBase : MonoBehaviour {

    private Transform _Transform;
    private float _nextFire;

    public float FireRate;
    public float ProjectilSpeed;
    public Transform Projectil;

	// Use this for initialization
	protected void Start () {
        _Transform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Fire(Vector2 vector)
    {
        if (Time.time < _nextFire)
            return;
        _nextFire = Time.time + FireRate;

        var projectil = Instantiate(Projectil, _Transform.position, Quaternion.identity);

        vector = new Vector2(vector.x, vector.y);
        vector.Normalize();
        vector = vector * ProjectilSpeed;

        projectil.GetComponent<Rigidbody2D>().velocity = vector;
        if(vector.x < 0){
            projectil.GetComponent<SpriteRenderer>().flipX = true;
        }
    }
}
