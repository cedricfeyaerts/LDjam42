﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PulsePowerUp : PowerUpBase {

    public float Force;
    public float Radius;

    // Use this for initialization
    public override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override bool Fire(Vector2 direction)
    {
        if (!base.Fire(direction))
            return false;

        var rigidBodies = FindObjectsOfType<Rigidbody2D>().Where(r => Vector2.Distance(_Rigidbody.position, r.position) <= Radius && r != _Rigidbody).ToList();

        foreach (var rigidBody in rigidBodies)
        {
            var force = new Vector2(rigidBody.position.x - _Rigidbody.position.x, rigidBody.position.y - _Rigidbody.position.y);
            force.Normalize();
            force *= Force;

            rigidBody.AddForce(force);
        }

        return true;
    }
}
