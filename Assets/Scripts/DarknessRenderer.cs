using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessRenderer : MonoBehaviour
{

    private MeshRenderer meshRenderer;
    private MeshFilter filter;
    private void Start () {
        // Set up game object with mesh;
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshRenderer.material = Resources.Load("Materials/Ground") as Material;
        print(meshRenderer.material);
        filter = gameObject.AddComponent<MeshFilter>();

    }

    public void render(Vector2[] vertices2D) {

        // // Create Vector2 vertices
        // var vertices2D = new Vector2[] {
        //     new Vector2(0,0),
        //     new Vector2(0,1),
        //     new Vector2(1,1),
        //     new Vector2(1,2),
        //     new Vector2(0,2),
        //     new Vector2(0,3),
        //     new Vector2(3,3),
        //     new Vector2(3,2),
        //     new Vector2(2,2),
        //     new Vector2(2,1),
        //     new Vector2(3,1),
        //     new Vector2(3,0),
        // };

        var vertices3D = System.Array.ConvertAll<Vector2, Vector3>(vertices2D, v => v);

        // Use the triangulator to get indices for creating triangles
        var triangulator = new Triangulator(vertices2D);
        var indices =  triangulator.Triangulate();

        // Generate a color for each vertex


        // Create the mesh
        var mesh = new Mesh {
            vertices = vertices3D,
            triangles = indices
        };

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        filter.mesh = mesh;

    }
}