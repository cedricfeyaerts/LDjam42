﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticuleViseeSouris : ReticuleViseeBase {
    
    private Transform _Transform;
    private Transform _Reticule;
    private Vector2 _Direction;

    public Transform Sprite;
    public float Distance;

	protected ReticuleViseeSouris()
    {
    }

    // Use this for initialization
	void Start () {
        _Transform = GetComponent<Transform>();

        var x = _Transform.position.x;
        var y = _Transform.position.y;

        var vector = new Vector3(x, y, 0);
        var distance = new Vector3(0, 1, 0);
        distance.Normalize();
        distance = distance * Distance;
        vector = vector + distance;
        _Reticule = Instantiate(Sprite, vector, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{
        var mouse_pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

        var rb_x = _Transform.position.x;
        var rb_y = _Transform.position.y;

        var vector = new Vector3(mouse_pos.x - rb_x, mouse_pos.y - rb_y);
        vector.Normalize();
        vector = vector * Distance;

        _Reticule.position = _Transform.position + vector;

        _Direction = vector;
	}

    public override Vector2 Direction()
    {
        return _Direction;
    }
}
