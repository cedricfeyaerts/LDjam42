﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tentacle : Ennemy {

    private float _radius = 1.0f;
    private float _timeBeforeRespawn = 4.0f;
    private Transform _transform;
    private Animator _animator;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        _animator = GetComponent<Animator>();
        InvokeRepeating("respawn", 0.0f, _timeBeforeRespawn);
        _transform = GetComponent<Transform>();
	}

	// Update is called once per frame
	void Update () {

	}

    void respawn() {
        _animator.SetTrigger("spawn");
        Vector2 position = Random.insideUnitCircle.normalized * _radius;
        _transform.position = new Vector2(Player.position.x + position.x ,Player.position.y + position.y);
    }
}
