﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenerateCircularMembrane : MonoBehaviour {

    private GameObject _GameObject;
    private List<Rigidbody2D> _Cells = new List<Rigidbody2D>();
    private List<BoxCollider2D> _Colliders = new List<BoxCollider2D>();

    public int CellNumber;
    public Vector2 Position;
    public float Rayon;
    public Rigidbody2D Prefab;
    public Rigidbody2D Player;

    public float Force;
    public float DistanceScaling;
    public float MaxForce;
    public float AttractionTreshold;

	// Use this for initialization
	void Start () {
        _GameObject = GetComponent<GameObject>();
        var lineRenderer = GetComponent<LineRenderer>();
        var increment = (2 * Mathf.PI) / CellNumber;
        for (int i = 1; i <= CellNumber; i++)
        {
            var x = Mathf.Cos(increment * i);
            var y = Mathf.Sin(increment * i);
            var vector = new Vector2(x, y);
            vector = vector * Rayon;
            vector = Position + vector;

            var cell = Instantiate(Prefab, vector, Quaternion.identity);

            var attractor = cell.GetComponent<AttractorComponent>();
            attractor.Force = Force;
            attractor.DistanceScaling = DistanceScaling;
            attractor.MaxForce = MaxForce;
            attractor.AttractionTreshold = AttractionTreshold;

            _Cells.Add(cell);
        }
        var count = _Cells.Count();
        foreach (var cell in _Cells)
        {
            var index = _Cells.IndexOf(cell);
            var next = (index + 1) % count;
            var previous = (index + count - 1) % count;

            var attractedBehavior = cell.GetComponent<AttractedBehavior>();
            attractedBehavior.Attractors.Add(_Cells[previous]);
            attractedBehavior.Attractors.Add(_Cells[next]);
            if (Player != null)
                attractedBehavior.Attractors.Add(Player);

            var colliderGameObject = new GameObject("LineCollider");
            var collider = colliderGameObject.AddComponent<BoxCollider2D>();
            var rigidBody = colliderGameObject.AddComponent<Rigidbody2D>();
            var ignoreMembrane = colliderGameObject.AddComponent<IgnoreCollision>();
            ignoreMembrane.Tag = "Membrane";
            colliderGameObject.AddComponent<OnProjectileCollision>();
            collider.transform.parent = lineRenderer.transform;
            collider.isTrigger = false;
            rigidBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            float width = lineRenderer.endWidth;
            var start = cell.position;
            var end = _Cells[next].position;
            float length = Vector3.Distance(start, end);
            collider.size = new Vector3(length, width, 0f);
            var midPoint = (start + end) / 2;
            collider.transform.position = midPoint;

            float angle = Mathf.Atan2(end.y - start.y, end.x - start.x);
            angle *= Mathf.Rad2Deg * -1;
            collider.transform.Rotate(0, angle, 0);
        }

        lineRenderer.positionCount = count + 1;
        lineRenderer.SetPositions(_Cells.Select(c => (Vector3)c.position).ToArray());
        lineRenderer.SetPosition(count, _Cells[0].position);
    }

    // Update is called once per frame
    void Update () {

	}

    private void FixedUpdate()
    {
        var lineRenderer = GetComponent<LineRenderer>();
        var positions = _Cells.Select(c => (Vector3)c.position).ToArray();
        lineRenderer.SetPositions(positions);
        lineRenderer.SetPosition(_Cells.Count(), _Cells[0].position);

        var count = _Cells.Count();

        foreach (var cell in _Cells)
        {
            var index = _Cells.IndexOf(cell);
            var next = (index + 1) % count;

            var collider = _Colliders[index];

            float width = lineRenderer.endWidth;
            var start = cell.position;
            var end = _Cells[next].position;
            float length = Vector3.Distance(start, end);
            collider.size = new Vector3(length, width, 0f);
            var midPoint = (start + end) / 2;
            collider.transform.position = midPoint;

            float angle = Mathf.Atan2(end.y - start.y, end.x - start.x);
            angle *= Mathf.Rad2Deg * -1;
            collider.transform.Rotate(0, angle, 0);
        }
    }
}
