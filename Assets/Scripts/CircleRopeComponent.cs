﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRopeComponent : MonoBehaviour {
	public Transform anchor;
	public Transform gear;
	public Transform player;
	public Transform tentacleDeco;

	LineRenderer lineRenderer;
	EdgeCollider2D edgeCollider;

	private List<RopeSection> sections = new List<RopeSection>();
	private List<Transform> gears = new List<Transform>();
	private List<Transform> decorations = new List<Transform>();
	private Vector2[] collisions;

	private float sectionLength = 1f;
	public int size = 30;
	public float radius = 4f / 2*Mathf.PI;
	public float lightFactor = 1.0f;
	public float darkFactor = 1.0f;

	// spring constant
    public float k = 2f;
    // spring dampening
    public float d = 1f;
    // air friction
    public float a = 0.7f;
	// mass
	public float m = 0.2f;
	public Transform darkness;

	void Start () {
		// lineRenderer = GetComponent<LineRenderer>();
		edgeCollider = GetComponent<EdgeCollider2D>();

		sectionLength = 2 * Mathf.PI * radius / size;

		List<Vector3> positions = new List<Vector3>();
		for (int i = 0; i < size; i++) {
			Vector3 p = Vector3.zero;
			p.x = anchor.position.x + radius * Mathf.Cos(i * 2 * Mathf.PI / size);
			p.y = anchor.position.y + radius * Mathf.Sin(i * 2 * Mathf.PI / size);
			positions.Add(p);
		}
		// add reversed to ease adding/removing segments (e.g for a winch)
		for (int i = positions.Count - 1; i >= 0; i--) {
			sections.Add(new RopeSection(positions[i]));
			gears.Add(Instantiate(gear, positions[i], Quaternion.identity));
			Transform deco;
			if (Random.value > 0.5f){
				deco = Instantiate(tentacleDeco, positions[0], Quaternion.identity);
				deco.localScale = new Vector3(0.25f+Random.value*0.5f,Random.value+0.3f, 1f);
			}else{
				var b = new GameObject();
				deco = b.transform;
			}
			decorations.Add(deco);
		}

		collisions = new Vector2[sections.Count];
		for (int i = sections.Count - 1; i >= 0; i--) {
			collisions[i] = Vector2.zero;
		}
	}

	void Update () {
		RenderRope();
		RenderDarkness();
	}

	void FixedUpdate() {
		if (sections.Count > 0) {
			int iterations = 1;
			float timeStep = Time.fixedDeltaTime / (float)iterations;
			for (int i = 0; i < iterations; i++) {
				UpdateRopeSimulation(sections, timeStep);
			}
			UpdateCollider(sections);
		}
	}

	void RenderRope() {
		// float width = 0.2f;
		// lineRenderer.startWidth = width;
		// lineRenderer.endWidth = width;

		Vector3[] positions = new Vector3[sections.Count + 1];

		for (int i = 0; i < sections.Count; i++) {
			positions[i] = sections[i].position;
		}
		positions[sections.Count] = sections[0].position;

		// lineRenderer.positionCount = positions.Length;
		// lineRenderer.SetPositions(positions);

		for (int i = 0; i < sections.Count; i++) {
			gears[i].position = sections[i].position;
			if (decorations[i]){
				decorations[i].position = sections[i].position;
				var direction = anchor.position - decorations[i].position;
				var angle = Mathf.Atan2(direction.x, direction.y)*Mathf.Rad2Deg;
				decorations[i].rotation = Quaternion.Euler(0, 0, -angle);
			}
		}
	}

	public void reduceLight(float value){
		darkFactor =  1.7f * value;
	}

	public void RenderDarkness() {

		// Vector2[] positions = new Vector2[sections.Count + 4];
		// positions[0] = Camera.main.ScreenToWorldPoint(new Vector3(0,            0,             0));
		// positions[1] = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0,             0));
		// positions[2] = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
		// positions[3] = Camera.main.ScreenToWorldPoint(new Vector3(0,            Screen.height, 0));

		Vector2[] positions = new Vector2[sections.Count];
		for (int i = 0; i < sections.Count; i++) {
			positions[i] = new Vector2( sections[i].position.x, sections[i].position.y);
		}

		darkness.GetComponent<DarknessRenderer>().render(positions);

    }
	private void UpdateCollider(List<RopeSection> sections) {
		if (edgeCollider == null) {
			return;
		}

		Vector3 centroid = anchor.transform.position;
		Vector2[] positions = new Vector2[sections.Count + 1];
		for (int i = 0; i < sections.Count; i++) {
			positions[i] = new Vector2(sections[i].position.x - centroid.x, sections[i].position.y - centroid.y);
		}
		positions[sections.Count] = new Vector2(sections[0].position.x, sections[0].position.y);
		edgeCollider.points = positions;
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Bullet")
		{
			float minDistance = Mathf.Infinity;
			int minIndex = 0;
			for (int i = 0; i < sections.Count; i++) {
				float distance = (collision.transform.position - sections[i].position).magnitude;
				if (distance < minDistance) {
					minDistance = distance;
					minIndex = i;
				}
			}
			collisions[minIndex] = collision.relativeVelocity;
			Destroy(collision.gameObject);
		}
	}

    #region physics

	private void UpdateRopeSimulation(List<RopeSection> sections, float timeStep) {
		// elastic collisions
		for (int i = 0; i < sections.Count; i++) {
			RopeSection section = sections[i];
			section.velocity.x = sections[i].velocity.x + collisions[i].x;
			section.velocity.y = sections[i].velocity.y + collisions[i].y;
			sections[i] = section;
			collisions[i] = Vector2.zero;
		}

		// forward Euler integrator
		List<Vector3> accelerations = ComputeAccelerations(sections);
		List<RopeSection> eNext = new List<RopeSection>();
		for (int i = 0; i < sections.Count; i++) {
			RopeSection section = RopeSection.zero;
			section.velocity = sections[i].velocity + accelerations[i] * timeStep;
			section.position = sections[i].position + sections[i].velocity * timeStep;
			eNext.Add(section);
		}

		// Heun's method (improved Euler)
		List<Vector3> eulerAccelerations = ComputeAccelerations(eNext);
		List<RopeSection> hNext = new List<RopeSection>();
		for (int i = 0; i < sections.Count; i++) {
			RopeSection section = RopeSection.zero;
			section.velocity = sections[i].velocity + (accelerations[i] + eulerAccelerations[i]) * 0.5f * timeStep;
			section.position = sections[i].position + (sections[i].velocity + eNext[i].velocity) * 0.5f * timeStep;
			hNext.Add(section);
		}

		for (int i = 0; i < sections.Count; i++) {
			//sections[i] = eNext[i];
			sections[i] = hNext[i];
		}

		int maxStretchIterations = 2;
		for (int i = 0; i < maxStretchIterations; i++) {
			ImplementMaximumStretch(sections);
		}

		anchor.position = Centroid();
	}

	private List<Vector3> ComputeAccelerations(List<RopeSection> sections) {
		List<Vector3> accelerations = new List<Vector3>();

		List<Vector3> forces = new List<Vector3>();

		// spring forces generated by each section
		for (int i = 0; i < sections.Count - 1; i++) {
			int j = i + 1;
			Vector3 vector = sections[j].position - sections[i].position;
			float distance = vector.magnitude;
			Vector3 direction = vector.normalized;

			float springForce = k * (distance - sectionLength);
			float dampeningForce = d * ((Vector3.Dot(sections[j].velocity - sections[i].velocity, vector)) / distance);

			Vector3 force = -(springForce + dampeningForce) * direction;
			force = -force; // looping backwards

			forces.Add(force);
		}
		// close the loop
		{
			int i = sections.Count - 1;
			int j = 0;
			Vector3 vector = sections[j].position - sections[i].position;
			float distance = vector.magnitude;
			Vector3 direction = vector.normalized;

			float springForce = k * (distance - sectionLength);
			float dampeningForce = d * ((Vector3.Dot(sections[j].velocity - sections[i].velocity, vector)) / distance);

			Vector3 force = -(springForce + dampeningForce) * direction;
			force = -force; // looping backwards

			forces.Add(force);
		}

		// compute acceleration from forces
		for (int i = 0; i < sections.Count; i++) {
			Vector3 springForce = Vector3.zero;
			// above
			springForce += forces[i];
			// below (except bottom since it has nothing below)
			if (i != 0) {
				springForce -= forces[i - 1];
			}

			Vector3 velocity = sections[i].velocity;
			Vector3 frictionForce = - a * velocity.magnitude * velocity.magnitude * velocity.normalized;

			float mass = m;
			Vector3 playerDirection = (player.position - sections[i].position).normalized;
			float playerMagniture = (player.position - sections[i].position).magnitude;
			if (playerMagniture < 2 * sectionLength) {
				playerMagniture = -1f / playerMagniture;
			} else {
				playerMagniture = 1f;
			}
			Vector3 playerForce = 9.81f * mass * playerMagniture * playerDirection * lightFactor;

			Vector3 selfForce = Vector3.zero;
			Vector3 centroid = anchor.transform.position;
			{
				Vector3 direction = (centroid - sections[i].position).normalized;
				float distance = -(centroid - sections[i].position).magnitude;
				selfForce = 7.5f / distance * darkFactor * direction;
			}

			Vector3 unfoldForce = Vector3.zero;
			{
				int prev = i == 0 ? sections.Count - 1 : i-1;
				int next = i == sections.Count - 1 ? 0 : i+1;
				Vector3 unfold = (sections[prev].position - sections[i].position) + (sections[next].position - sections[i].position);
				Vector3 unfoldDirection = unfold.normalized;
				float unfoldMagnitude = Mathf.Exp(unfold.magnitude);
				unfoldForce = unfoldMagnitude * unfoldDirection;
			}

			Vector3 totalForce = springForce + playerForce + selfForce + frictionForce + unfoldForce;
			Vector3 acceleration = totalForce / mass;
			accelerations.Add(acceleration);
		}

		// top doesn't move
		accelerations.Add(Vector3.zero);

		return accelerations;
	}

	private Vector3 Centroid() {
		float area = 0f;
		Vector3 centroid = Vector3.zero;
		for (int i = 0; i < sections.Count - 1; i++) {
			int j = i + 1;
			float d = sections[i].position.x * sections[j].position.y - sections[j].position.x * sections[i].position.y;
			centroid.x += (sections[i].position.x + sections[j].position.x) * d;
			centroid.y += (sections[i].position.y + sections[j].position.y) * d;

			area += d;
		}
		{
			int i = sections.Count - 1;
			int j = 0;
			float d = sections[i].position.x * sections[j].position.y - sections[j].position.x * sections[i].position.y;
			area += d;
			centroid.x += (sections[i].position.x + sections[j].position.x) * d;
			centroid.y += (sections[i].position.y + sections[j].position.y) * d;
		}


		area /= 2f;
		centroid.x /= 6 * area;
		centroid.y /= 6 * area;

		return centroid;
	}

	private void ImplementMaximumStretch(List<RopeSection> sections) {
		float max = 2.0f;
		float min = 0.1f;

		for (int i = sections.Count - 1; i > 0; i--) {
			int j = i - 1;
			RopeSection top = sections[i];
			RopeSection bottom = sections[j];

			float dist = (top.position - bottom.position).magnitude;
			float stretch = dist / sectionLength;
			if (stretch > max) {
				float length = dist - (sectionLength * max);
				Vector3 direction = (top.position - bottom.position).normalized;
				Vector3 change = direction * length;
				MoveSection(change, j);
			} else if (stretch < min) {
				float length = (sectionLength * min) - dist;
				Vector3 direction = (bottom.position - top.position).normalized;
				Vector3 change = direction * length;
				MoveSection(change, j);
			}
		}
		// close the loop
		{
			int i = 0;
			int j = sections.Count - 1;
			RopeSection top = sections[i];
			RopeSection bottom = sections[j];

			float dist = (top.position - bottom.position).magnitude;
			float stretch = dist / sectionLength;
			if (stretch > max) {
				float length = dist - (sectionLength * max);
				Vector3 direction = (top.position - bottom.position).normalized;
				Vector3 change = direction * length;
				MoveSection(change, j);
			} else if (stretch < min) {
				float length = (sectionLength * min) - dist;
				Vector3 direction = (bottom.position - top.position).normalized;
				Vector3 change = direction * length;
				MoveSection(change, j);
			}
		}
	}

	private void MoveSection(Vector3 change, int index) {
		RopeSection bottom = sections[index];
		Vector3 position = bottom.position;
		position += change;
		bottom.position = position;
		sections[index] = bottom;
	}

	#endregion
}
